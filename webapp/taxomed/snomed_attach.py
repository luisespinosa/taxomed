#!/usr/bin/python
# -*- coding: utf-8 -*-

import pickle
import os
import sys
import logging
import numpy as np
from collections import defaultdict, Counter

import gensim

import networkx as nx
from networkx.readwrite import json_graph

def predict_attachment(term, taxograph, model, vocabulary, blacklist, use_probabilities=True, was_def=False):
	r"""Predicts the most likely attachment in snomed for a given term.
	
	Parameters
	----------
	term : string
		Term to be predicted
	taxograph : nx.DiGraph
		Taxonomy graph
	model : Word2Vec
		Embeddings model after: (1) model=gensim.models.Word2Vec.load(vectorspath); (2) model=model.wv
	vocabulary : 
		Embeddings vocabulary, should come from word2vec model: model.wv.index2word
		
	Returns
	-------
	attachments : list
		Sorted array of most likely attachments in snomed    
	"""
	
	if is_snomed(taxograph, term) and not was_def:
		
			successors = taxograph.successors(term)
			
			subgraph = get_adjacent_subgraph(taxograph, term)
			data = json_graph.node_link_data(subgraph)
			
			return successors[:10]
		
	else:
		most_similar = get_most_similar(term, model, vocabulary)
		candidates = get_attachment_candidates(taxograph, most_similar, blacklist, use_probabilities)
		
		logging.info('For term: %s ', term)
		logging.info('Similar words in space: %s', most_similar)
		logging.info('Most likely attachments: ')
		logging.info('Freq\tTerm')
		for k, v in candidates:
			logging.info("%s\t%s", v, k)
			
		return candidates[:10]

test=[k for k in """trastorno
conducta
costes
hipótesis
interacción
endoscopista
frenología
poliglobulia
anexión
anticoagulación
beta-caroteno
septicemia
genisteína
sarcopenia
apolipoproteinas
""".split() if k]

if __name__ == '__main__':

	args=sys.argv[1:]

	if len(args) == 2:

		vectorspath=args[0]
		taxopath=args[1]
		
		model=gensim.models.Word2Vec.load(vectorspath)
		wv=model.wv

		taxo=read_taxo(taxopath)
		terminology=set(taxo.keys())

		# TO-DO #
		G = nx.DiGraph(taxo)

		for term in test:
			if term in wv.index2word:
				most_sim=[k for k,v in wv.most_similar(term)]
				attachs=defaultdict(int)
				for t in most_sim:
					if t in G.nodes():
						neighbours=list(G[t].keys()) # get 1st level predecessors and successors
						for n in neighbours:
							attachs[n]+=1
					else:
						print('Discarding: ',t)
				sortattachs=sorted(attachs.items(), key=lambda x:x[1], reverse=True)
				print('For term: ',term)
				print('Similar words in space: ',most_sim)
				print('Most likely attachments: ')
				for k,v in sortattachs:
					print(k,'\tscore:',v)
			else:
				print('This word not in embeddings: ',term)
			input('*******')
