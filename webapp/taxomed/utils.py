import networkx as nx
import pickle
import os
import sys
import gensim
import logging
import numpy as np
from collections import defaultdict
import json

def read_taxo(taxopath):
	print('Loading taxo: ', taxopath)
	out=defaultdict(set)
	for line in open(taxopath,'r'):
		cols=line.decode('utf8').strip().split('\t')
		if len(cols) == 2:
			out[cols[1]].add(cols[0])
	return out

def get_terminology(taxonomy):
	return set(taxo.keys())

def taxo2graph(taxodict):
	graph = nx.DiGraph(taxodict)
	return graph


def load_taxonomy(taxopath):
	print('(new) Loading taxo: ', taxopath)

	fd = open(taxopath,'r')
	lines = fd.readlines()
	fd.close()

	graph = nx.DiGraph()
	try:
		idx = 0
		parents = []
		previous_child = None
		while idx < len(lines):

			line = lines[idx]
			child, parent = line.decode('utf8').strip().split('\t')
			
			if previous_child and previous_child == child:
				
				if parent in graph:
					
					ancestors = nx.ancestors(graph, parent)
					for previous_parent in parents:
						
						if previous_parent in ancestors:
							parents.remove(previous_parent)
					
					add_parent = True
					for previous_parent in parents:
						ancestors = nx.ancestors(graph, parent)
						if parent in ancestors:
							add_parent = False
							break
					
					if add_parent:
						parents.append(parent)
				
				else:
					graph.add_node(parent)
			
			else:
				if previous_child:
					graph.add_node(previous_child)
					for parent in parents:
						graph.add_edge(parent, previous_child)
				
				parents = [parent]
				previous_child = child
				
			idx += 1
			print "Reading line ", idx, "\r",
	
	except:
	
		data = nx.node_link_data(graph)
		json_data = json.dumps(data)
		fd = open('data/snomed_graph_filtered.json', 'w')
		fd.write(json_data)
		fd.close()
		
		nx.write_edgelist(graph, 'data/snomed_graph_filtered.txt', data=False)
	
	return graph
	
def load_embeddings(path):
	print('Loading embeddings: ', path)
	model=gensim.models.Word2Vec.load(path)
	wv=model.wv
	return wv,wv.index2word

def pad(sent, maxlen=20, test=False):
	if len(sent) > maxlen:
		padded_sent=sent[:maxlen]
	else:
		padded_sent=[]
		for tok in sent:
			padded_sent.append(tok) 
		dif=maxlen - len(sent)
		for i in range(dif):
			if not test:
				padded_sent.append(('UNK',-1))
			else:
				padded_sent.append('UNK')
	return padded_sent

def vectorize(tokens, wvmodel, wvvocab, wvdims, maxlen=20):
	out=[]
	if len(tokens) > maxlen:
		for token in tokens[:maxlen]:
			if token in wvvocab:
				out.append(wvmodel[token])
			else:
				out.append(np.zeros(wvdims))
	else:
		for token in tokens[:maxlen]:
			if token in wvvocab:
				out.append(wvmodel[token])
			else:
				out.append(np.zeros(wvdims))		
		dif = maxlen-len(tokens)
		for i in range(dif):
			out.append(np.zeros(wvdims))
	return np.array(out)
