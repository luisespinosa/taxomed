#!/usr/bin/python
# -*- coding: utf-8 -*-

import codecs
import logging

import numpy as np
from collections import Counter

import networkx as nx
from networkx.readwrite import json_graph, tree_data

# imports luis
import utils
from keras.models import load_model
# /imports luis

EMBEDDINGS_PATH = 'data/scielo_wikies_wv2'
TAXONOMY_PATH = 'data/snomed_graph_new.txt' #'data/snomed_transitive_closure.txt_descriptions.txt'
HYPERNYM_MODEL = 'data/conv_lstm_hypers'
BLACKLIST_PATH = 'data/blacklist500.txt'

class TaxoGraph(object):
	
	taxograph = None
	blacklist = None

	def __init__(self):
		
		_class = self.__class__
		
		if _class.taxograph is None:
			print 'Loading taxonomy...'
			
			#"""
			taxonomy = utils.read_taxo(TAXONOMY_PATH)
			taxograph = utils.taxo2graph(taxonomy)
			"""
			taxograph = utils.load_taxonomy(TAXONOMY_PATH)
			#"""
			_class.taxograph = taxograph
		else:
			print 'Taxonomy already loaded!'
			
		if _class.blacklist is None:
			print 'Loading blacklist...'
			_class.blacklist = [line.split('\t')[0] for line in codecs.open(BLACKLIST_PATH,'r', 'utf8')]
			
	
	def is_snomed(self, term):
		return term in self.taxograph
	
	def get_adjacent_nodes(self, term):
		r"""Retrieves the predecessors and successors of the term in the input graph.
		
		Parameters
		----------
		term : string
			Term to be queried
		taxograph : nx.DiGraph
			Taxonomy graph
			
		Returns
		-------
		adjacent : list
			List of adjacent terms.
		"""
		
		adjacent = []
		
		predecessors = self.taxograph.predecessors(term)
		adjacent.extend(predecessors)
		
		successors = self.taxograph.successors(term)
		adjacent.extend(successors)
		
		return adjacent
		
	def get_adjacent_tree_data(self, term):
		r"""Construct the subtree of adjacents nodes to the term.
		
		Parameters
		----------
		taxograph : nx.DiGraph
			Taxonomy graph
		term : string
			Term to be queried
			
		Returns
		-------
		subgraph : nx.DiGraph
			Subgraph around term.
		"""
		adjacent = []
		
		predecessors = list(self.taxograph.predecessors(term))[:1]
		adjacent.extend(predecessors)
		
		# presuccessors = list(self.taxograph.successors(predecessors[0]))[:10]
		# adjacent.extend(presuccessors)
		
		successors = list(self.taxograph.successors(term))[:10]
		adjacent.extend(successors)
		
		if term not in successors:
			adjacent.append(term)
		
		subgraph = nx.DiGraph.subgraph(self.taxograph, adjacent)
		return tree_data(subgraph, predecessors[0], {'id':'name', 'children': 'children'})
		
	def get_adjacent_subgraph(self, term):
		r"""Construct the subgraph of adjacents nodes to the term.
		
		Parameters
		----------
		taxograph : nx.DiGraph
			Taxonomy graph
		term : string
			Term to be queried
			
		Returns
		-------
		subgraph : nx.DiGraph
			Subgraph around term.
		"""
		adjacent = []
		
		predecessors = list(self.taxograph.predecessors(term))[:5]
		adjacent.extend(predecessors)
		
		successors = list(self.taxograph.successors(term))[:5]
		adjacent.extend(successors)

		adjacent.append(term)
		
		return nx.DiGraph.subgraph(self.taxograph, adjacent)
		
	def get_attachment_candidates(self, terms, use_probabilities=True):
		r"""Retrieves adjacent nodes to the input terms, returning a frecuency list.
		
		Parameters
		----------
		terms : probability dict
			Dictionary of terms with it's probabilities
		taxograph : nx.DiGraph
			Taxonomy graph
			
		Returns
		-------
		sorted_adjacents : list
			Sorted frecuency list. 
		"""
		
		counter = Counter()
		for term in terms:
			
			if term in self.taxograph.nodes():
			
				if use_probabilities:
					factor = terms[term]
				else:
					factor = 1
				
				adjacent = self.get_adjacent_nodes(term) # get 1st level predecessors and successors
				for adj_term in adjacent:
					#adj_term=adj_term.encode('utf-8')
					if not adj_term in self.blacklist:
						counter.update({adj_term:factor})
					else:
						logging.warn('Discarding %s, too generic!', adj_term)
			else:
				logging.warn('Discarding %s, not found in snomed!', term)
		
		sorted_adjacents = counter.most_common()
		
		return sorted_adjacents

class Taxomed(object):

	model = None
	vocabulary = None
	hyp_model = None

	def __init__(self):
		
		_class = self.__class__
		
		if _class.model is None:
			print 'Loading embeddings...'
			model, vocabulary = utils.load_embeddings(EMBEDDINGS_PATH)
			_class.model = model
			_class.vocabulary = vocabulary
			
		else:
			print 'Embeddings already loaded!'

		if _class.hyp_model is None:
			print 'Loading keras model...'
			_class.hyp_model = load_model(HYPERNYM_MODEL)
			
	def get_most_similar(self, term):
		r"""Retrieves the most similar terms in the word2vec model, if present.
		
		Parameters
		----------
		term : string
			Term to be queried
		model : Word2Vec
			Embeddings model after: (1) model=gensim.models.Word2Vec.load(vectorspath); (2) model=model.wv
		vocabulary : 
			Embeddings vocabulary, should come from word2vec model: model.wv.index2word
			
		Returns
		-------
		most_similar : dict
			Dict of most similar terms in the word2vec model along with the probability.
		"""
		
		most_similar = []
		
		term_tokens = term.split()
		if len(term_tokens) == 1:
			
			if term in self.vocabulary:
				most_similar = self.model.most_similar(term)
			else:
				logging.warn('El término %s no está en el vocabulario.', term)

		else:
			tokens_to_avg=[]
			for token in term_tokens:
				if token in self.vocabulary:
					tokens_to_avg.append(self.model[token])
			
			if tokens_to_avg:
				avg = np.mean(np.array(tokens_to_avg),axis=0)
				most_similar = self.model.most_similar(positive=[avg],topn=20)
				
			else:
				logging.warn('Ningún token de "%s" se ha encontrado en el modelo!', term)
				
		logging.warn('For term: %s',term)
		logging.warn('These are the most similar: %s',str(most_similar))
		
		return dict(most_similar)
		
	def predict_hypernim(self, definition):
		
		maxlen=20
		dims=100

		logging.info('def: %s ', definition)

		tokens = definition.split()
		logging.info('tokens:  %s', tokens)

		def_tokens = utils.pad(tokens, maxlen)
		logging.info('padded:  %s', def_tokens)

		vec_tokens = utils.vectorize(def_tokens, self.model, self.vocabulary, dims,maxlen)
		logging.info('vectorized:  %s', vec_tokens)

		pred = list(self.hyp_model.predict(np.array([vec_tokens]))[0])
		logging.info('Prediction:  %s', pred)
		
		combined = zip(tokens, pred)
		prediction = [dict(zip(('word', 'attention'), (values[0], float(values[1])))) for values in combined]
		logging.info('Prediction dict:  %s', prediction)
		
		hypernym = tokens[list(pred).index(max(pred))]
		logging.info('hypernym:  %s', hypernym)
		
		return hypernym, prediction



"""
maxlen=20
dims=100

nnmode=load_model(HYPERNYM_MODEL)

nnmodel=load_model('data/conv_lstm_hypers')
tokens='La gonartrosis o artrosis de rodilla, consiste en la artrosis localizada en la articulación de la rodilla'.lower().split()
tokens=utils.pad(tokens,maxlen)
vec_tokens=vectorize(tokens,_class.model,_class.vocab,dims,maxlen)
pred=list(nnmodel.predict(np.array([vec_tokens]))[0])
>>> print([(token,pred[idx]) for idx,token in enumerate(tokens)])
>>> [('la', 0.010850927), ('gonartrosis', 0.035442255), ('o', 0.014526689), ('artrosis', 0.38634074), ('de', 0.17833327), ... ]
"""
