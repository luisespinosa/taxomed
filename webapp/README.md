# Taxomed webapp

Para hacer funcionar el servicio es necesario tener instaladas las siguientes dependencias de python:
* flask
* Flask-WTF

Para arrancar el servicio web solo es necesario lanzar el siguiente comando:
python webapp.py

Una vez arranque el servicio, podrá ver la interfaz web accediendo mediante un navegador a http://localhost:5555 
