#!/usr/bin/python
# -*- coding: utf-8 -*-

import json

import logging
logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)

from flask import Flask, render_template, flash, request

from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField
from wtforms.widgets import TextArea, CheckboxInput
from wtforms.validators import DataRequired

from taxomed.taxomed import Taxomed, TaxoGraph

import numpy as np
from networkx.readwrite import json_graph

# App config.
DEBUG = True
app = Flask(__name__)
app.config.from_object(__name__)
app.config['SECRET_KEY'] = '7d442f27d443f27667d4a1f2b6176a'
 
class TermQueryForm(FlaskForm):
    term = StringField('Term', validators=[DataRequired()])
    definition = StringField('Definition', widget=TextArea())
    use_probabilities = BooleanField('Use probabilities')

@app.route("/", methods=['GET', 'POST'])
def index():
    single_list = None
    multiple_list = None
    def_attention = []
    graph_json = {}
    form = TermQueryForm(request.form)

    # print "Errors: ", form.errors
    if form.is_submitted():
        if form.validate_on_submit(): 
            
            # So something
            term = request.form['term']
            print 'Term: ',term
            
            definition = request.form['definition'].strip()
            print 'Definition: ',definition
            
            use_probabilities = 'use_probabilities' in request.form
            print 'Probabilities: ',use_probabilities

            taxomed = Taxomed()
            taxograph = TaxoGraph()
            
            
            # si el termino esta en snomed, sacamos el subgrafo (sobreescribiendo el del hiperonimo)
            if taxograph.is_snomed(term):
                flash('The term %s exists in snomed!' % term)
                
                # graph_json = taxograph.get_adjacent_tree_data(term)
            
            else:

                hypernym = None
                # Si hay definicion
                if definition:
                    # predecimos probabilidades de hiperonimo
                    hypernym, prediction = taxomed.predict_hypernim(definition)
                    # asignamos la prediccion a la variable del heat_map
                    def_attention = prediction
                    
                    # sustituimos term por el hiperonimo, para buscar los most similar de este
                    term = hypernym
                    
                # buscamos la lista de most similar y sus attachment candidates
                most_similar = taxomed.get_most_similar(term)
                logging.info('For term: %s ', term)
                logging.info('Similar words in space: %s', most_similar)

                candidates = sorted(list(most_similar.items()), key=lambda x: x[1], reverse=True)
                # candidates = taxograph.get_attachment_candidates(most_similar, use_probabilities=use_probabilities)
                
                logging.debug('Most likely attachments: ')
                logging.debug('Freq\tTerm')
                for idx in range(len(candidates)):
                    candidate, score = candidates[idx]
                    
                    logging.debug("%s\t%s", candidate, score)
                    if taxograph.is_snomed(candidate):
                        candidates[idx] = (candidate, score, 'background-color: lavender')
                    else:
                        candidates[idx] = (candidate, score, 'background-color: inherit')
                        
                
                if hypernym:
                    # y si esta en snomed, sacamos el subgrafo
                    if taxograph.is_snomed(hypernym):
                        flash('The hypernim detected %s exists in snomed!' % hypernym)
                        
                        # graph_json = taxograph.get_adjacent_tree_data(hypernym)
                        
                        candidates.insert(0, (hypernym, "-"))
                
                multiple_list = candidates[:10]

                multiple_list.insert(0, ("Term", "Score", "background-color: #265a88; color: white; background-image: linear-gradient(to bottom,#337ab7 0,#265a88 100%);"))

        else:
            flash('Error: Term form field is required. ')
    
    # graph_json = {}
             
    return render_template('index.html', form=form, single_list=single_list, multiple_list=multiple_list, graph_json=json.dumps(graph_json), def_attention=json.dumps(def_attention))

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5555)
