#!/usr/bin/python
# -*- coding: utf-8 -*-

TAXONOMY_PATH = 'data/sorted_snomed_transitive.txt'
OUTPUT_PATH = 'data/snomed_graph_new.txt'
REST_PATH = 'data/snomed_graph_rest.txt'

def next_parsed_line(line):
    try :
        child, parent = line.strip().split('\t')
        return child, parent
    except:
        print idx, repr(line)
        raise
        
def remove_bad_lines(lines):
    idx = 0
    while idx < len(lines):
        line = lines[idx]
        try :
            initial_child, _ = next_parsed_line(line)
            idx += 1
        except:
            print "Skipping: ", idx, line,
            lines.pop(idx)
            continue
            
    return lines

def get_candidates(lines, nodes):
    
    idx = 0
    idxs = []

    candidate_idxs = set()
    previous_node = None
    initial_idx = idx
    while idx < len(lines):

        line = lines[idx]
        child, parent = next_parsed_line(line)
        
        if previous_node and previous_node == child:
            #raw_input("equals")
            if parent in nodes:
                candidate_idxs.add(idx)
        
        else:
            #raw_input("not equals")

            if len(candidate_idxs) == idx - initial_idx:
                idxs.extend(candidate_idxs)
                #raw_input("inserting set...")

                """
                if idx - initial_idx > 1:
                    print visited_parents
                    raw_input("Multiple visited parents found!")
                """
            elif previous_node and idx - initial_idx == 1:
                #print lines[initial_idx]
                #raw_input("inserting...")

                idxs.append(initial_idx)
            
            candidate_idxs = set()
            if parent in nodes:
                candidate_idxs.add(idx)
            
            previous_node = child
            initial_idx = idx

        idx += 1

    return idxs

def remove_parent(lines, added_parents):   
         
    node_parents = {}
    
    idx = 0 
    previous_node = None
    while idx < len(lines):
        
        line = lines[idx]
        child, parent = next_parsed_line(line)
        
        if previous_node and previous_node == child:
            if parent in added_parents or parent in added_parents.values():
                node_parents[parent] = idx
        
        else:
            to_pop = []
            for node_parent in node_parents:
                
                if node_parent in added_parents:
                    ancestor = added_parents[node_parent]
                    
                    #print node_parents
                    #print lines[node_parents[node_parent]]
                    #print lines[node_parents[ancestor]]
                    #raw_input("Check!")
                    
                    if ancestor in node_parents:
                        to_pop.append(node_parents[ancestor])
                    
            while to_pop:
                lines.pop(to_pop.pop())
                idx -= 1

                
            node_parents = {}
            if parent in added_parents or parent in added_parents.values():
                node_parents[parent] = idx
            previous_node = child

        idx += 1
        print "Removing ", idx, "\r",
            
    to_pop = []
    for node_parent in node_parents.keys():
        
        if node_parent in added_parents.keys():
            ancestor = added_parents[node_parent]
            
            #print node_parents
            #print lines[node_parents[node_parent]]
            #print lines[node_parents[ancestor]]
            #raw_input("Check!")
            
            if ancestor in node_parents:
                to_pop.append(node_parents[ancestor])
            
    while to_pop:
        lines.pop(to_pop.pop())
        idx -= 1

if __name__ == "__main__":

    fd = open(TAXONOMY_PATH,'r')
    out_fd = open(OUTPUT_PATH, 'w')
    
    nodes = set()
    
    lines = fd.readlines()
    lines = remove_bad_lines(lines)
    while lines:
        print "Getting candidates... ", len(lines)
        idxs = get_candidates(lines, nodes)
        
        if not idxs:
            print 
            print "There are still ", len(lines), " lines"
            print 
            
            rest_fd = open(REST_PATH, 'w')
            for line in lines:
                rest_fd.write(line)
            
            fd.close()
            out_fd.close()
            rest_fd.close()
            
            raise Exception("No single parents found!")
        
        print "Removing lines...", len(idxs)
        added_parents = {}
        while idxs:
            idx = idxs.pop()
            line = lines.pop(idx)
            
            child, parent = line.strip().split('\t')
            added_parents[child] = parent
            
            nodes.add(child)
            nodes.add(parent)

            out_fd.write(line)
            #print "Adding %s" % line
        
        remove_parent(lines, added_parents)
            
    fd.close()
    out_fd.close()
