# Taxomed
## II HACKATHON DE TECNOLOG�AS DEL LENGUAJE

__TaxoMed__ es un sistema que permite enriquecer y extender autom�ticamente bases de datos terminol�gicas, tesauros y taxonom�as l�xicas, optimizado para el dominio biom�dico. El sistema se basa en dos ideas fundamentales:

* Dado un t�rmino de nuevo cu�o y definici�n asociada, un modelo basado en deep learning aisla hiper�nimo.
* En caso de no existir definici�n, explota proximidad en un espacio vectorial derivado de un corpus en crudo.

Este repositorio contiene el c�digo necesario para desplegar la herramienta __TaxoMed__. 
Puedes descargar el contenido del repositorio en formato .zip desde esta web o usando un cliente git.

En el caso de usar un cliente git debe poder usar [git-lfs](https://git-lfs.github.com/).
Una vez comprobado esto, podr�s clonar el repositorio:

```git clone https://luisespinosa@bitbucket.org/luisespinosa/taxomed.git```

Y desde el directorio base del repositorio, descargar los modelos:

```git lfs pull```

Para poner en marcha el servicio, ejecuta desde el directorio base del repositorio: 

```python webapp/webapp.py```

Este comando iniciar� el servicio en el puerto 5555 de localhost y accediendo a http://localhost:5555 mediante un navegador podr�s ver la intefaz de Taxomed.

### Experimentos relacionados con los distintos m�dulos de TaxoMed se pueden consultar en:

__Extracci�n de hiper�nimos y generaci�n de taxonom�as__

Espinosa-Anke, L., Saggion, H., Ronzano, F., & Navigli, R. (2016, February). Extasem! extending, taxonomizing and semantifying domain terminologies. In Proceedings of the 30th Conference on Artificial Intelligence (AAAI�16).

__Modelado y comportamiento de hiper�nimos en corpus__

Espinosa-Anke, L., Camacho-Collados, J., Bovi, C. D., & Saggion, H. (2016). Supervised distributional hypernym discovery via domain adaptation. In Proceedings of the 2016 Conference on Empirical Methods in Natural Language Processing (pp. 424-435).

__Enriquecimiento de Snomed__

Espinosa-Anke, L, Tello, J, Pardo, A, Medrano, I, Ure�a, A, Salcedo, I, Saggion, H. Savana: A global information extraction and terminology expansion framework in the medical domain. Procesamiento del Lenguaje Natural. 2016;57: 23-30.

__Enriquecimiento de WordNet__

Espinosa-Anke, L., Camacho-Collados, J., Rodr�guez Fern�ndez, S., Saggion, H., & Wanner, L. (2016). Extending WordNet with fine-grained collocational information via supervised distributional learning. In Proceedings of COLING 2016: Technical Papers. Osaka (Japan). COLING; 2016. p. 900-10.

### Contacto
_espinosa-ankel@cardiff.ac.uk - roberto.carlini@upf.edu_